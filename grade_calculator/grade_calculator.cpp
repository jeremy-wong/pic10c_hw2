#include "grade_calculator.h"
#include <algorithm>

const int MAX_HW = 20;
const int MAX_EXAM = 100;

grade_calculator::grade_calculator(QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.button1->setChecked(true);

	QObject::connect(ui.hw1_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw2_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw3_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw4_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw5_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw6_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw7_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.hw8_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.midterm1_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.midterm2_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.final_slide, SIGNAL(valueChanged(int)),
		this, SLOT(update_overall()));
	QObject::connect(ui.button1, SIGNAL(clicked(bool)),
		this, SLOT(update_overall()));

}

void grade_calculator::update_overall() {
	// double score = 31.4;
	double hw1 = ui.hw1_slide->value();
	double hw2 = ui.hw2_slide->value();
	double hw3 = ui.hw3_slide->value();
	double hw4 = ui.hw4_slide->value();
	double hw5 = ui.hw5_slide->value();
	double hw6 = ui.hw6_slide->value();
	double hw7 = ui.hw7_slide->value();
	double hw8 = ui.hw8_slide->value();
	double midterm1 = ui.midterm1_slide->value();
	double midterm2 = ui.midterm2_slide->value();
	double final_score = ui.final_slide->value();

	double score = (hw1 + hw2 + hw3 + hw4
		+ hw5 + hw6 + hw7 + hw8) / (MAX_HW * 8) * 25;

	bool scheme = ui.button1->isChecked();
	if (scheme) {
		score += midterm1 / MAX_EXAM * 20 + midterm2 / MAX_EXAM * 20
			+ final_score / MAX_EXAM * 35;
	}
	else {
		score += std::max(midterm1, midterm2) / MAX_EXAM * 30
			+ final_score / MAX_EXAM * 44;
	}
	
	ui.label->setText(QString::number(score));

	return;
}