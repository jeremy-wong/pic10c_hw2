#pragma once

#include <QtWidgets/QWidget>
#include "ui_grade_calculator.h"

class grade_calculator : public QWidget
{
	Q_OBJECT

	public:
		explicit grade_calculator(QWidget *parent = Q_NULLPTR);

	signals:
		void compute_overall();

	public slots:
		void update_overall();

	private:
		Ui::grade_calculatorClass ui;
};
